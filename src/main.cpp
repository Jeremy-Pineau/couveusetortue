#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#define brocheDeBranchementDHT 8   // Sensor data on digital pin 8
#define typeDeDHT DHT22 // Sensor Type: DHT22
#define R 11 // Red LED connected to pin 11
#define V 12 // Green LED connected to pin 12
#define B 13 // Blue LED connected to pin 13

// Defines the type of lcd screen 16 columns x 2 lines with I2C address 0x27
LiquidCrystal_I2C lcd(0x27,16,2); 

// DHT library instantiation
DHT dht(brocheDeBranchementDHT, typeDeDHT);

// Initialize DHT22
void InitializeDHT22() 
{
  Serial.begin(9600);
  
  dht.begin();
}

// Initialize LCD
void InitializeLCD()
{
  lcd.init();
  lcd.backlight();
}

// Initialize LED
void InitializeLED(){
  pinMode(B, OUTPUT); // Declare the pin number associated with LEDPIN as an output = pinMode(13, OUTPUT);
  pinMode(V, OUTPUT); // Declare the pin number associated with LEDPIN as an output = pinMode(12, OUTPUT);
  pinMode(R, OUTPUT); // Declare the pin number associated with LEDPIN as an output = pinMode(11, OUTPUT);
}

// Disable all leds
void AllLedsLow(){
    digitalWrite(B,LOW);
    digitalWrite(V,LOW);
    digitalWrite(R,LOW);
}

// Set blue led on high
void BlueLedHigh(){
    digitalWrite(B,HIGH);
    digitalWrite(V,LOW);
    digitalWrite(R,LOW);
}

// Set green led on high
void GreenLedHigh(){
    digitalWrite(B,LOW);
    digitalWrite(V,HIGH);
    digitalWrite(R,LOW);
}

// Set red led on high
void RedLedHigh(){
    digitalWrite(B,LOW);
    digitalWrite(V,LOW);
    digitalWrite(R,HIGH);
}

// Check if we receive data from DHT22
bool VerifyDataDHT22(){

  // Reading of the humidity rate (in %)
  float tauxHumidite = dht.readHumidity(); 

  // Temperature reading, expressed in degrees Celsius
  float temperatureEnCelsius = dht.readTemperature();  

  if (isnan(tauxHumidite) || isnan(temperatureEnCelsius)) {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("COMPOSANT");
    lcd.setCursor(0,1);
    lcd.print("MANQUANT");
    AllLedsLow();
    delay(2000);
    return false;   
  }

  delay(2000);
  return true;
}

void PrintAllData(){
  
  // Reading of the humidity rate (in %)
  float tauxHumidite = dht.readHumidity(); 

  // Temperature reading, expressed in degrees Celsius
  float temperatureEnCelsius = dht.readTemperature();  

  if((tauxHumidite > 50) & (tauxHumidite < 85))
{
  // Clears the contents of the LCD screen
  lcd.clear();
  
  if((temperatureEnCelsius >= 25) & (temperatureEnCelsius < 29))
  {
    lcd.setCursor(0,0);
    lcd.print("");
    lcd.print("Sexe : Male");
  }
  else if(temperatureEnCelsius < 25)
  {
    BlueLedHigh();
  }
  
  if((temperatureEnCelsius >= 29) & (temperatureEnCelsius < 31))
  {
    lcd.setCursor(0,0);
    lcd.print("");
    lcd.print("Sexe : M et F");
  }

  if((temperatureEnCelsius >= 31) & (temperatureEnCelsius <32))
  {
    lcd.setCursor(0,0);
    lcd.print("");
    lcd.print("Sexe : Femele");
  }
  else if (temperatureEnCelsius > 32)
  {
    RedLedHigh();
  }

  if((temperatureEnCelsius >= 25) & (temperatureEnCelsius < 32))
  {
    GreenLedHigh();
  }
}
else
{
  lcd.setCursor(0,0);
  if(tauxHumidite <= 75)
  {
    lcd.print("HUMIDITY TO LOW");
  }
  else
  {
    lcd.print("HUMIDITY TO HIGH");
  }

  AllLedsLow();
}

  // Display of temperature and humidity values
  lcd.setCursor(0,1);
  lcd.print("H=");
  lcd.print(tauxHumidite,1); // Displays the value with one digit after the decimal point
  lcd.print("%");

  lcd.print(" T=");
  lcd.print(temperatureEnCelsius,1); // Displays the value with one digit after the decimal point
  lcd.print((char)223);
  lcd.print("C");
  
  // 2 second delay
  //(as a reminder: do not try to do more than 1 reading every 2 seconds, with the DHT22, according to the manufacturer)
  delay(2000);
  }

void setup() 
{
  InitializeDHT22();
  InitializeLCD();
  InitializeLED();
}

void loop() 
{
  if(VerifyDataDHT22())
  {
    PrintAllData();
  }
}